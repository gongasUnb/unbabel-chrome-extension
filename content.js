// not the best practice
var username = ''
var password = ''
var languagesArray = {}
chrome.storage.sync.get(null, function (data) {
	if (data.username !== '' && data.password !== '') {
		username = data.username
		password = data.password
		init()
	}
});
var email = ''


function init() {
	// gets the array of languages and stores it in the array
	getLanguagesArray(function (response) {
		languagesArray = JSON.parse(response);
	})

	InboxSDK.load('1', 'sdk_extensionTest_495d79310a').then(function (sdk) {
		//get the user email
		email = sdk.User.getEmailAddress()
		// the SDK has been loaded, now do something with it!
		sdk.Compose.registerComposeViewHandler(function (composeView) {

			// adds a button in composeView
			composeView.addButton({
				title: "Translate",
				iconUrl: chrome.extension.getURL('images/unbabel.png'),
				onClick: function (event) {
					let finalText = event.composeView.getTextContent();
					let text = event.composeView.getSelectedBodyHTML();
					if (text === '') {
						text = finalText
					}
					
					// not the best practice but its working for now
					setTimeout(function () {
						addBodyPopover(text, finalText, email, composeView);
					}, 50);
				},
			});

			// adds popover function over the button
			addPopover();
			initializePopover();
		});

	});
}
