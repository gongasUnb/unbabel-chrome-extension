chrome.runtime.onInstalled.addListener(function () {
  chrome.storage.sync.set(
    {
      username: '',
      password: '',
      isLoggedIn: false,
      defaultTo: '',
      defaultFrom: ''
    }, function () {
      chrome.storage.sync.get(null, function (data) {
        console.log('updated: ' + data.username + ' and ' + data.password);
      });
    });
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
    chrome.declarativeContent.onPageChanged.addRules([{
      conditions: [new chrome.declarativeContent.PageStateMatcher({
        pageUrl: { hostEquals: 'mail.google.com' },
      })
      ],
      actions: [new chrome.declarativeContent.ShowPageAction()]
    }]);
  });
});

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request.message === 'login') {
    var manifest = chrome.runtime.getManifest();

    var clientId = encodeURIComponent(manifest.oauth2.client_id);
    var scopes = encodeURIComponent(manifest.oauth2.scopes.join(' '));
    //var redirectUri = encodeURIComponent(chrome.identity.getRedirectURL("oauth2"));
    var redirectUri = encodeURIComponent('https://unbabel-chrome-extension.herokuapp.com/oauth2')
    var url = 'https://accounts.google.com/o/oauth2/auth' +
      '?client_id=' + clientId +
      '&response_type=code' +
      '&access_type=offline' +
      '&include_granted_scopes=true' +
      '&redirect_uri=' + redirectUri +
      '&scope=' + scopes
    console.log(url)
    chrome.identity.launchWebAuthFlow(
      {
        'url': url,
        'interactive': true
      },
      function (redirectedTo) {
        if (chrome.runtime.lastError) {
          // Example: Authorization page could not be loaded.
          console.log(chrome.runtime.lastError.message);
        }
        else {
          console.log(redirectedTo)
          sendResponse(redirectedTo);
        }
      }
    );
  }
  else if(request.message === 'save'){

  }
})