// initialize boostrap for popover usage
function initializePopover() {
	// this is needed for the popover to work
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	});
	$(function () {
		$('[data-toggle="popover"]').popover()
	})
}

// finds the button and adds the popover for the button
function addPopover() {
	const buttons = document.querySelectorAll(".inboxsdk__composeButton");
	for (let i = 0; i < buttons.length; i++) {
		if (buttons[i].getAttribute('aria-label') == 'Translate') {
			var btn = buttons[i];
			btn.id = 'btnPopup';
			btn.setAttribute('data-toggle', 'popover')
			btn.setAttribute('data-placement', 'top')
			btn.setAttribute('data-title', 'Unbabel')
			btn.setAttribute('data-html', true)
			break;
		}
	}
}

// gets languages from server
function getLanguagesArray(callback) {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			if (callback) {
				callback(xhttp.responseText);
			}
		}
	};
	xhttp.open("GET", "https://unbabel-chrome-extension.herokuapp.com/languages", true);
	xhttp.setRequestHeader('Authorization', `ApiKey ${username}:${password}`)
	xhttp.send();
}

// adds available options to the select
// value 0 is "from", value 1 is "to"
function addOptions(select, value) {
	chrome.storage.sync.get(null, function (data) {
		if (value === 1) {
			addDefaultOptions(select, data.defaultTo)
		}
		else {
			addDefaultOptions(select, data.defaultFrom)
		}
	});
}
function addDefaultOptions(select, defaultOption){
	for (i in languagesArray) {
		//create a list of options to select from
		let option = document.createElement('option');
		option.value = i; //shortname
		option.text = languagesArray[i]; //name

		//if shortname is default then select it
		if (i === defaultOption){
			option.selected = 'true'
		}

		//adds options to selects
		select.add(option);
	}
}

// dynamicaly adds the body to the popover
function addBodyPopover(text, fullText, email, composeView) {
	const popovers = document.querySelectorAll(".popover.fade.bs-popover-top.show");
	for (let i = 0; i < popovers.length; i++) {
		var el = popovers[i].childNodes;
		//check if title is the same name
		if (el[1].nodeName == 'H3' && el[1].textContent == 'Unbabel') {
			var div = document.createElement('div');

			//label for the "Translate From:" dropdown
			var label1 = document.createElement('label');
			var textNode1 = document.createTextNode('Translate From:');
			label1.setAttribute('for', 'transFrom');
			label1.appendChild(textNode1);

			//label for the "Translate To:" dropdown
			var label2 = document.createElement('label');
			var textNode2 = document.createTextNode('Translate To:');
			label2.setAttribute('for', 'transTo');
			label2.appendChild(textNode2);

			//create a dropdown to select languages to translate from
			var select1 = document.createElement('select');
			select1.setAttribute('name', 'translateFrom');
			select1.id = 'transFrom';

			//create a dropdown to select languages to translate to
			var select2 = document.createElement('select');
			select2.setAttribute('name', 'translateTo');
			select2.id = 'transTo';

			// adds available options to the selects
			addOptions(select1, 0);
			addOptions(select2, 1);

			//add the "translate" button
			var button = document.createElement('button');
			button.id = 'btnUnbabel'
			button.addEventListener("click", () => {
				getUserInput(text, fullText, email, composeView, false);
			});
			button.innerText = 'Translate'

			//add the "translate and send" button
			var button2 = document.createElement('button');
			button2.id = 'btnUnbabel2'
			button2.addEventListener("click", () => {
				getUserInput(text, fullText, email, composeView, true); //true is to auto send
			});
			button2.innerText = 'Translate and Send'

			//add a switch option to select from human or AI translation
			var div1 = document.createElement('div');
			div1.className = "custom-control custom-switch";
			var input1 = document.createElement('input');
			input1.className = "custom-control-input";
			input1.id = "switch1";
			input1.setAttribute('type', "checkbox");
			var label3 = document.createElement('label');
			label3.className = "custom-control-label"
			var textNode3 = document.createTextNode('Human Translation');
			label3.setAttribute('for', 'switch1');
			label3.appendChild(textNode3);
			div1.appendChild(input1);
			div1.appendChild(label3);

			//add a switch option to save the languages options
			var div2 = document.createElement('div');
			div2.className = "custom-control custom-switch";
			var input2 = document.createElement('input');
			input2.className = "custom-control-input";
			input2.id = "switch2";
			input2.setAttribute('type', "checkbox");
			var label4 = document.createElement('label');
			label4.className = "custom-control-label"
			var textNode4 = document.createTextNode('Save Languages');
			label4.setAttribute('for', 'switch2');
			label4.appendChild(textNode4);
			div2.appendChild(input2);
			div2.appendChild(label4);

			//add everything to the main div
			div.appendChild(label1);
			div.appendChild(select1);
			div.appendChild(label2);
			div.appendChild(select2);
			div.appendChild(div1);
			div.appendChild(div2);
			div.appendChild(button);
			div.appendChild(button2);

			// el[2] is the popover body
			el[2].appendChild(div)
		}
	}

	// this reframes the popover automatically
	$(document).ready(function () {
		$('[data-toggle="popover"]').popover('update');
	});
}

// gets the options the user selected in the dropdowns
async function getUserInput(text, fullText, email, composeView, autoSendOption) {
	// get the "from" option 
	var from = $('#transFrom')
		.children("option:selected").val();
	// get the "to" option 
	var to = $('#transTo')
		.children("option:selected").val();
	// get the option for AI/Human
	var translationOption = $("#switch1").prop('checked');

	//save languages option if user clicked
	var saveOption = $("#switch2").prop('checked');
	if (saveOption) {
		saveOptions(from, to);
	}

	// get the draftID
	var draftId = await composeView.getDraftID();

	var json = {
		"text": text,
		"fullText": fullText,
		"source_language": from,
		"target_language": to,
		"username": username,
		"password": password,
		"draftId": draftId,
		"email": email
	}
	//console.log(json)
	translate(json, translationOption, autoSendOption, composeView);
}

// returns the translated text and calls the callback if exists
function manipulateResponse(response, composeView) {
	var json, text, fullText, translated, final

	// get the variables values
	json = JSON.parse(response)
	text = json.text
	fullText = json.fullText
	translated = json.translated

	// if text is empty the translation will be the full text
	if (text === '') {
		composeView.setBodyText(translated);
	}
	// else replace the full text with the text translated
	else {
		fullText = fullText.replace(text, translated)
		composeView.setBodyText(fullText)
	}
}

// create a message
function createMessage(message) {
	const popovers = document.querySelectorAll(".popover.fade.bs-popover-top.show");
	for (let i = 0; i < popovers.length; i++) {
		var el = popovers[i].childNodes;
		//check if title is the same name
		if (el[1].nodeName == 'H3' && el[1].textContent == 'Unbabel') {
			let div = document.createElement('div');
			let label = document.createElement('label');
			let node = document.createTextNode(message);

			label.appendChild(node);
			div.appendChild(label);

			// el[2] is the popover body
			el[2].appendChild(div)
		}
	}

	// this reframes the popover automatically
	$(document).ready(function () {
		$('[data-toggle="popover"]').popover('update');
	});
}

function closePopover() {
	$(function () {
		$('[data-toggle="popover"]').popover('hide')
	})
}

function saveOptions(from, to) {
	console.log('saving: ' + from + '/' + to)
	chrome.storage.sync.set(
		{
			defaultFrom: from,
			defaultTo: to
		});
}