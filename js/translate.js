function translate(json, isHumanTranslation, sendAuto, composeView) {
    if (!isHumanTranslation) {
        postAI(json, composeView);
    }
    else {
        postHuman(json, sendAuto, composeView);
    }
}

function postAI(json, composeView) {
    let xhttp = new XMLHttpRequest();
    let username = json.username, password = json.password;
    delete json['username'];
    delete json['password'];
    let params = JSON.stringify(json);
    xhttp.open('POST', "https://unbabel-chrome-extension.herokuapp.com/translate_mt", true);

    //Send the proper header information along with the request
    xhttp.setRequestHeader('Content-type', 'application/json');
    xhttp.setRequestHeader('Authorization', `ApiKey ${username}:${password}`)
    xhttp.onreadystatechange = function () {//Call a function when the state changes.
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            manipulateResponse(xhttp.responseText, composeView)
        }
    }
    xhttp.send(params);
}

function postHuman(json, sendAuto, composeView) {
    let arr = composeView.getToRecipients()
    if (sendAuto && Array.isArray(arr) && arr.length === 0){
        createMessage('Please enter a valid To recipient')
        return
    }
    let xhttp = new XMLHttpRequest();
    let username = json.username, password = json.password;
    delete json['username'];
    delete json['password'];
    encodedEmail = encodeURIComponent(json['email'])
    json["callback_url"] = `https://unbabel-chrome-extension.herokuapp.com/receive_callback/${json['draftId']}/${encodedEmail}/${btoa(unescape(encodeURIComponent(json['fullText'])))}/${sendAuto}`;
    console.log(json["callback_url"]);
    let params = JSON.stringify(json);
    xhttp.open('POST', "https://unbabel-chrome-extension.herokuapp.com/translate_human", true);

    //Send the proper header information along with the request
    xhttp.setRequestHeader('Content-type', 'application/json');
    xhttp.setRequestHeader('Authorization', `ApiKey ${username}:${password}`)
    xhttp.onreadystatechange = function () {

        if (xhttp.readyState == 4) {//Call a function when the state changes.

            if (xhttp.status == 200) { //if server returns ok status
                console.log(xhttp.responseText);

                if (sendAuto) { //if is autosend then close the popover and the compose view
                    closePopover()
                    setTimeout(function () {
                        composeView.close()
                    }, 50);
                } 
                else {
                    createMessage('Message will be processed, please close this popup!');
                }

            }
            else if (xhttp.status == 400) {
                console.log(xhttp.responseText);
                createMessage(xhttp.responseText);
            }
        }

    }
    xhttp.send(params);
}