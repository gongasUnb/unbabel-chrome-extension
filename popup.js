var form = document.getElementById('loginForm')
var btn = document.getElementById('buttonGet')

form.onsubmit = function (e) {
    var newUsername = document.getElementById('username').value;
    var newPassword = document.getElementById('password').value;
    chrome.storage.sync.set(
        {
            username: newUsername,
            password: newPassword
        }, function () {
            chrome.storage.sync.get(null, function (data) {
                console.log('updated: ' + data.username + ' and ' + data.password);
                alert('New data has been saved!')
            });
        });
}

btn.onclick = function (e) {
    chrome.storage.sync.get(null, function (data) {
        console.log('actual: ' + data.username + ' and ' + data.password);
    });
}

document.getElementById("auth")
    .addEventListener('click', function () {
        chrome.runtime.sendMessage({ message: 'login' });
    });